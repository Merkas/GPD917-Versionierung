﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Versionierung
{
    /// <summary>
    /// Contains Code to check whether the Game is over and who won if there is a winner
    /// </summary>
    class GameOverChecker
    {
        public bool CheckIfGameIsOver(FieldState[,] fieldStates)
        {
            if (CheckIfFieldIsFilled(fieldStates))
                return true;

            return false;
        }

        /// <summary>
        /// Loops through an array. If it contains a FieldState.Empty returns false, else returns true.
        /// </summary>
        /// <param name="fieldStates">array of FieldState (Enum) which displays the game board and its states</param>
        /// <returns>true when the field doesn't contain a FieldState.Empty</returns>
        private bool CheckIfFieldIsFilled(FieldState[,] fieldStates)
        {
            for (int i = 0; i < fieldStates.GetLength(0); i++)
            {
                for (int j = 0; j < fieldStates.GetLength(1); j++)
                {
                    if (fieldStates[i, j] == FieldState.Empty)
                        return false;
                }
            }

            return true;
        }



        
    }
}
