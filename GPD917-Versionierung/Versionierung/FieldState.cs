﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Versionierung
{
    /// <summary>
    ///     Possible states for each field of the game grid
    /// </summary>
    public enum FieldState : byte
    {
        Empty,
        Cross,
        Circle
    }
}
