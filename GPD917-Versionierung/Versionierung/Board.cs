﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Versionierung
{
    /// <summary>
    ///     Represents the board area
    /// </summary>
    public class Board
    {
        /// <summary> Current state of the board </summary>
        public FieldState[,] fieldStates;

        /// <summary> The dimensions of the board (width and height) </summary>
        public const int DIMENSIONS = 3;

        /// <summary> How wide and high (characters and lines) the displayed board is </summary>
        public const int DISPLAY_DIMENSIONS = 13;

        /// <summary> Background text to use for displaying </summary>
        private const string DISPLAY_BACKGROUND =
            "+---+---+---+\n" +
            "|   |   |   |\n" +
            "|   |   |   |\n" +
            "|   |   |   |\n" +
            "+---+---+---+\n" +
            "|   |   |   |\n" +
            "|   |   |   |\n" +
            "|   |   |   |\n" +
            "+---+---+---+\n" +
            "|   |   |   |\n" +
            "|   |   |   |\n" +
            "|   |   |   |\n" +
            "+---+---+---+";

        /// <summary> Character to use for an empty field </summary>
        private const char FIELD_EMPTY = ' ';

        /// <summary> Character to use as a cross </summary>
        private const char FIELD_CROSS = 'X';

        /// <summary> Character to use as a circle </summary>
        private const char FIELD_CIRCLE = 'O';

        /// <summary> Distance between displayed marks </summary>
        private const int DISPLAY_DISTANCE = 3;

        /// <summary> How far the marks are offset from the top left of board </summary>
        private const int DISPLAY_OFFSET = 2;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Board"/> class.
        /// </summary>
        public Board()
        {
            fieldStates = new FieldState[DIMENSIONS, DIMENSIONS];
        }

        /// <summary>
        ///     Display the board.
        ///     Overwrites any overlapping console text.
        /// </summary>
        /// <param name="x">Left X Coordinate (Character)</param>
        /// <param name="y">Top Y Coordiante (Line)</param>
        public void Display(int x = 0, int y = 0)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(DISPLAY_BACKGROUND);

            for (int _x = 0; _x < DIMENSIONS; _x++)
            {
                for (int _y = 0; _y < DIMENSIONS; _y++)
                {
                    Console.SetCursorPosition(_x + x + DISPLAY_OFFSET, _y + y + DISPLAY_OFFSET);

                    var fieldState = fieldStates[_x, _y];
                    Console.Write(
                        fieldState == FieldState.Cross ? FIELD_CROSS :
                        fieldState == FieldState.Circle ? FIELD_CIRCLE :
                        FIELD_EMPTY
                        );
                }
            }
        }
    }
}
